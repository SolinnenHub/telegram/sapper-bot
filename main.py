import asyncio, logging, uuid
from random import randrange
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.contrib.middlewares.logging import LoggingMiddleware
import config as cfg

logging.basicConfig(level=logging.WARNING)

bot = Bot(token=cfg.botToken)
dp = Dispatcher(bot)
dp.middleware.setup(LoggingMiddleware())

# игры раздельны для каждого пользователя
# доступ к словарю осуществляется по уникальному id пользователя (чата)
# подразумевается, что бота нельзя будет добавлять в группы, в противном случае делать ходы сможет каждый.
#
# В BotFather есть опция, позволяющая запретить добавление бота в групповые чаты
games = {}

SIZE = 3
MINES = 2

# челвоеко-читаемые значения в массиве flatten_grid
UNKNOWN_NO_MINE = 0
UNKNOWN_MINE = -1
KNOWN_NO_MINE = 1
KNOWN_MINE = -2

ICONS = {
	UNKNOWN_NO_MINE: '?',
	UNKNOWN_MINE: '?',
	KNOWN_NO_MINE: ' ',
	KNOWN_MINE: '💥'
}


def hasUnknownEmptyFields(flatten_grid):
	for item in flatten_grid:
		if item == UNKNOWN_NO_MINE:
			return True
	return False


def expose(flatten_grid):
	for i in range(len(flatten_grid)):
		if flatten_grid[i] == UNKNOWN_NO_MINE:
			flatten_grid[i] = KNOWN_NO_MINE
		elif flatten_grid[i] == UNKNOWN_MINE:
			flatten_grid[i] = KNOWN_MINE
	return flatten_grid


async def render(chat_id, original_message=None): # обновить сообщение с кнопками
	markup = InlineKeyboardMarkup()
	pos = 0
	for i in range(SIZE):
		row = []
		for j in range(SIZE):
			icon = ICONS[games[chat_id]['flatten_grid'][pos]] # выбираем иконку из словаря ICONS
			# в каждой кнопке зашита строка callback_data, 
			# при нажатии она отправляется в метод callback, описанный ниже
			row.append(InlineKeyboardButton(icon, callback_data=f'{pos}|{games[chat_id]["uuid"]}'))
			pos += 1
		markup.row(*row)

	# если метод вызван на основе другого сообщения (сообщения с кнопками)
	if original_message:
		await original_message.edit_reply_markup(markup)
	else: # если предыдущее сообщение не определено (а значит, мы здесь из-за команды /start)
		await bot.send_message(chat_id, "Игра:", reply_markup=markup)


@dp.message_handler(commands=['start']) # срабатывает при выполнении команды /start
async def handler(message: types.Message):

	# одномерный массив, который описывает каждый элемент из сетки
	flatten_grid = [UNKNOWN_NO_MINE]*(SIZE**2) 
	mines_left = MINES

	assert mines_left <= SIZE**2 # количество мин не должно превышать число ячеек

	# расставляем мины случайным образом в flatten_grid
	while mines_left > 0:
		r = randrange(SIZE**2)
		if flatten_grid[r] == 0:
			flatten_grid[r] = UNKNOWN_MINE
			mines_left -= 1

	chat_id = message.chat.id
	games[chat_id] = {
		'is_over': False,
		'uuid': str(uuid.uuid4()),
		'flatten_grid': flatten_grid
	}

	await render(chat_id, original_message=None)

# функция срабатывает при нажатии на кнопку
@dp.callback_query_handler()
async def callback(query: types.CallbackQuery):
	args = query.data.split('|')

	pos = int(args[0])
	game_uuid = args[1]

	chat_id = query.message.chat.id

	# запрещаем одновременно проводить несколько игр, для этого нужен game_uuid, который также спрятан в каждую кнопку.
	if not chat_id in games or \
			games[chat_id]['uuid'] != game_uuid:
		return await query.answer("Ошибка: эта игра недействительна, попробуйте начать новую при помощи /start.")

	if games[chat_id]['is_over']:
		return await query.answer("Игра окончена, попробуйте начать новую при помощи /start.")

	if games[chat_id]['flatten_grid'][pos] == KNOWN_NO_MINE:
		return await query.answer()
	elif games[chat_id]['flatten_grid'][pos] == KNOWN_MINE:
		return await query.answer()

	elif games[chat_id]['flatten_grid'][pos] == UNKNOWN_MINE:
		games[chat_id]['flatten_grid'][pos] = KNOWN_MINE
		games[chat_id]['is_over'] = True
		msg = "Вы проиграли! Используйте /start, чтобы начать новую игру."
		await query.answer(msg)
		await bot.send_animation(chat_id, open('files/lost.mp4', 'rb'), caption=msg)
		games[chat_id]['flatten_grid'] = expose(games[chat_id]['flatten_grid'])
		await render(chat_id, original_message=query.message)

	elif games[chat_id]['flatten_grid'][pos] == UNKNOWN_NO_MINE:
		games[chat_id]['flatten_grid'][pos] = KNOWN_NO_MINE
		if not hasUnknownEmptyFields(games[chat_id]['flatten_grid']):
			games[chat_id]['is_over'] = True
			games[chat_id]['flatten_grid'] = expose(games[chat_id]['flatten_grid'])
			msg = "Вы победили! Используйте /start, чтобы начать новую игру."
			await query.answer(msg)
			await bot.send_animation(chat_id, open('files/won.mp4', 'rb'), caption=msg)
		
		await render(chat_id, original_message=query.message)


if __name__ == '__main__':
	loop = asyncio.get_event_loop()
	executor.start_polling(dp, loop=loop, skip_updates=True)

# sapper-bot

Игра «Сапёр», реализованная в Telegram Боте

## Развертывание

```bash
conda env create -f environment.yml
conda activate sapper-bot
python main.py
```
